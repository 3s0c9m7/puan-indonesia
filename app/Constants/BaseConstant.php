<?php

namespace App\Constants;

abstract class BaseConstant
{
    abstract public static function labels(): array;

    public static function label($id = false): ?string
    {
        if ($id === false || is_null($id)) {
            return null;
        }

        return static::labels()[$id] ?? '';
    }

    public static function labelData($id = false): string
    {
        return static::labels()[$id] ?? '-';
    }
}
