@extends('website::layouts.admin.app')

@php
@endphp
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('List Blog') }}</div>

                <div class="card-body">
                    {{-- add new blog--}}
                    <div class="d-grid gap-2 d-md-flex justify-content-md-start">
                        <a class="btn btn-outline-primary" href="{{ route('admin.blog.create') }}" role="button">Add New Blog</a>
                    </div>

                    {{-- filter list blogs --}}
                    <form action="">
                        <div class="row g-3 justify-content-end">
                            <div class="col-auto">
                                <input name="search" type="text" class="form-control" value="{{ request('search') }}" placeholder="Search input">
                            </div>
                            <div class="col-auto">
                                <button type="submit" class="btn btn-outline-secondary">Search</button>
                            </div>
                        </div>
                    </form>

                    {{-- list blogs --}}
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Thumbnail</th>
                                <th>Title</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($blogs as $blog)
                            @php $imageUrl = $blog->image ? asset('storage/blogs/'.$blog->image) : 'https://fakeimg.pl/20x30'; @endphp
                            <tr>
                                <td><img src="{{ $imageUrl }}" class="img-thumbnail" alt="thumbnail" width="50px"></td>
                                <td>{{ $blog->title }}</td>
                                <td>
                                    <div class="row g-3">
                                        <div class="col-auto">
                                            <a class="btn btn-outline-info" href="{{ route('admin.blog.edit', $blog->uuid) }}" role="button">Edit</a>
                                        </div>
                                        <div class="col-auto">
                                            <a class="btn btn-outline-danger" href="{{ route('admin.blog.delete', $blog->uuid) }}" role="button">Delete</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @empty

                            @endforelse
                        </tbody>
                    </table>
                    {{ $blogs->appends(request()->query())->links() }}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
