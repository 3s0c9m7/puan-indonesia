@extends('website::layouts.admin.app')

@php
@endphp
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Product') }}</div>

                <div class="card-body">
                    {{-- add new product--}}
                    <div class="d-grid gap-2 d-md-flex justify-content-md-start">
                        <a class="btn btn-outline-primary" href="{{ route('admin.master.product.create') }}" role="button">Add New Product</a>
                    </div>

                    {{-- list product --}}
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Product Type</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($products as $product)
                            @php $imageUrl = $product->image ? asset('storage/products/'.$product->image) : 'https://fakeimg.pl/300x100'; @endphp
                            <tr>
                                <td>{{ $product->name }}</td>
                                <td>{{ optional($product->type)->title ?? '-' }}</td>
                                <td>
                                    <div class="row g-3">
                                        <div class="col-auto">
                                            <a class="btn btn-outline-info" href="{{ route('admin.master.product.edit', $product->uuid) }}" role="button">Images</a>
                                        </div>
                                        <div class="col-auto">
                                            <a class="btn btn-outline-info" href="{{ route('admin.master.product.edit', $product->uuid) }}" role="button">Edit</a>
                                        </div>
                                        <div class="col-auto">
                                            <a class="btn btn-outline-danger" href="{{ route('admin.master.product.delete', $product->uuid) }}" role="button">Delete</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @empty

                            @endforelse
                        </tbody>
                    </table>
                    {{ $products->appends(request()->query())->links() }}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
