@extends('website::layouts.admin.app')

@php
@endphp
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Edit Product') }}</div>

                <div class="card-body">
                    {{-- form input blog --}}
                    <form method="post" action="{{ route('admin.master.product.update', $product->uuid) }}" enctype="multipart/form-data">
                        @csrf
                        <div class="mb-3">
                            <label for="inputTitle" class="form-label">Name</label>
                            <input name="name" value="{{ old('name', $product->name) }}" placeholder="Input name" type="text" class="form-control" id="inputTitle" aria-describedby="emailHelp">
                            @error('title')
                                <div class="text-danger">{{ $errors->first('title') }}</div>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="inputProductType" class="form-label">Product Type</label>
                            <select name="product_type_id" class="form-control" id="inputProductType">
                                <option value="" selected disabled>Pilih Product Type</option>
                                @foreach($productTypes as $type)
                                <option value="{{ $type->id }}" {{ old('product_type_id', $product->product_type_id) == $type->id ? 'selected' : '' }}>{{ $type->title }}</option>
                                @endforeach
                            </select>
                            @error('product_type_id')
                                <div class="text-danger">{{ $errors->first('product_type_id') }}</div>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="inputDescription" class="form-label">Description</label>
                            <textarea name="description" class="form-control" id="inputDescription" rows="3" placeholder="Input description">{{ old('description', $product->description) }}</textarea>
                            @error('description')
                                <div class="text-danger">{{ $errors->first('description') }}</div>
                            @enderror
                        </div>

                        <div class="row g-3 justify-content-end">
                            <div class="col-auto">
                                <a class="btn btn-outline-secondary" href="{{ route('admin.master.product.index') }}" role="button">Cancel</a>
                            </div>
                            <div class="col-auto">
                                <button type="submit" class="btn btn-outline-primary">Save</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('styles')
    <script src="https://cdn.ckeditor.com/ckeditor5/41.4.2/classic/ckeditor.js"></script>
@endpush

@push('scripts')
<script>
    ClassicEditor.create(document.querySelector('#inputDescription')).catch(error => {
        console.error(error);
    });
</script>
@endpush
