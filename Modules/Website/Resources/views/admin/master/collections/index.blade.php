@extends('website::layouts.admin.app')

@php
@endphp
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Product Collection') }}</div>

                <div class="card-body">
                    {{-- add new collection--}}
                    <div class="d-grid gap-2 d-md-flex justify-content-md-start">
                        <a class="btn btn-outline-primary" href="{{ route('admin.master.collection.create') }}" role="button">Add New Product Collection</a>
                    </div>

                    {{-- list collection --}}
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($collections as $collection)
                            @php $imageUrl = $collection->image ? asset('storage/collections/'.$collection->image) : 'https://fakeimg.pl/300x100'; @endphp
                            <tr>
                                <td>{{ $collection->name }}</td>
                                <td>
                                    <div class="row g-3">
                                        <div class="col-auto">
                                            <a class="btn btn-outline-info" href="{{ route('admin.master.collection.edit', $collection->uuid) }}" role="button">Edit</a>
                                        </div>
                                        <div class="col-auto">
                                            <a class="btn btn-outline-danger" href="{{ route('admin.master.collection.delete', $collection->uuid) }}" role="button">Delete</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @empty

                            @endforelse
                        </tbody>
                    </table>
                    {{ $collections->appends(request()->query())->links() }}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
