@extends('website::layouts.admin.app')

@php
@endphp
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Add New Product Collection') }}</div>

                <div class="card-body">
                    {{-- form input product --}}
                    <form method="post" action="{{ route('admin.master.collection.store') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="mb-3">
                            <label for="inputTitle" class="form-label">Name</label>
                            <input name="name" value="{{ old('name') }}" placeholder="Input name" type="text" class="form-control" id="inputTitle" aria-describedby="emailHelp">
                            @error('name')
                                <div class="text-danger">{{ $errors->first('name') }}</div>
                            @enderror
                        </div>

                        <div class="row g-3 justify-content-end">
                            <div class="col-auto">
                                <a class="btn btn-outline-secondary" href="{{ route('admin.master.collection.index') }}" role="button">Cancel</a>
                            </div>
                            <div class="col-auto">
                                <button type="submit" class="btn btn-outline-primary">Save</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('styles')
    <script src="https://cdn.ckeditor.com/ckeditor5/41.4.2/classic/ckeditor.js"></script>
@endpush

@push('scripts')
<script>
    ClassicEditor.create(document.querySelector('#inputDescription')).catch(error => {
        console.error(error);
    });
</script>
@endpush
