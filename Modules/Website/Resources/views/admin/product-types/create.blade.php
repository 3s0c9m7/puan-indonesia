@extends('website::layouts.admin.app')

@php
@endphp
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Add New Product Type') }}</div>

                <div class="card-body">
                    {{-- form input slideshow --}}
                    <form method="post" action="{{ route('admin.master.product_type.store') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="mb-3">
                            <label for="inputTitle" class="form-label">Title</label>
                            <input name="title" value="{{ old('title') }}" placeholder="Input title" type="text" class="form-control" id="inputTitle" aria-describedby="emailHelp">
                            @error('title')
                                <div class="text-danger">{{ $errors->first('title') }}</div>
                            @enderror
                        </div>

                        <div class="row g-3 justify-content-end">
                            <div class="col-auto">
                                <a class="btn btn-outline-secondary" href="{{ route('admin.slideshow.index') }}" role="button">Cancel</a>
                            </div>
                            <div class="col-auto">
                                <button type="submit" class="btn btn-outline-primary">Save</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('styles')
@endpush

@push('scripts')
@endpush
