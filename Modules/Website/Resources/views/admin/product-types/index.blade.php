@extends('website::layouts.admin.app')

@php
@endphp
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Product Type') }}</div>

                <div class="card-body">
                    {{-- add new productType--}}
                    <div class="d-grid gap-2 d-md-flex justify-content-md-start">
                        <a class="btn btn-outline-primary" href="{{ route('admin.master.product_type.create') }}" role="button">Add New Product Type</a>
                    </div>

                    {{-- list Product Type --}}
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($productTypes as $productType)
                            <tr>
                                <td>{{ $productType->title }}</td>
                                <td>
                                    <div class="row g-3">
                                        <div class="col-auto">
                                            <a class="btn btn-outline-info" href="{{ route('admin.master.product_type.edit', $productType->uuid) }}" role="button">Edit</a>
                                        </div>
                                        <div class="col-auto">
                                            <a class="btn btn-outline-danger" href="{{ route('admin.master.product_type.delete', $productType->uuid) }}" role="button">Delete</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @empty

                            @endforelse
                        </tbody>
                    </table>
                    {{ $productTypes->appends(request()->query())->links() }}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
