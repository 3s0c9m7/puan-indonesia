@extends('website::layouts.admin.app')

@php
@endphp
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Slideshow') }}</div>

                <div class="card-body">
                    {{-- add new slideshow--}}
                    <div class="d-grid gap-2 d-md-flex justify-content-md-start">
                        <a class="btn btn-outline-primary" href="{{ route('admin.slideshow.create') }}" role="button">Add New Slideshow</a>
                    </div>

                    {{-- list slideshow --}}
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Thumbnail</th>
                                <th>Title</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($slideshows as $slideshow)
                            @php $imageUrl = $slideshow->image ? asset('storage/slideshows/'.$slideshow->image) : 'https://fakeimg.pl/300x100'; @endphp
                            <tr>
                                <td><img src="{{ $imageUrl }}" class="img-thumbnail" alt="thumbnail" width="300px"></td>
                                <td>{{ $slideshow->title }}</td>
                                <td>
                                    <div class="row g-3">
                                        <div class="col-auto">
                                            <a class="btn btn-outline-info" href="{{ route('admin.slideshow.edit', $slideshow->uuid) }}" role="button">Edit</a>
                                        </div>
                                        <div class="col-auto">
                                            <a class="btn btn-outline-danger" href="{{ route('admin.slideshow.delete', $slideshow->uuid) }}" role="button">Delete</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @empty

                            @endforelse
                        </tbody>
                    </table>
                    {{ $slideshows->appends(request()->query())->links() }}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
