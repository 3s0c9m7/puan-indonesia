@extends('website::layouts.admin.app')

@php
@endphp
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Edit Slideshow') }}</div>

                <div class="card-body">
                    {{-- form input slideshow --}}
                    <form method="post" action="{{ route('admin.slideshow.update', $slideshow->uuid) }}" enctype="multipart/form-data">
                        @csrf
                        <div class="mb-3">
                            <label for="inputTitle" class="form-label">Title</label>
                            <input name="title" value="{{ old('title', $slideshow->title) }}" placeholder="Input title" type="text" class="form-control" id="inputTitle" aria-describedby="emailHelp">
                            @error('title')
                                <div class="text-danger">{{ $errors->first('title') }}</div>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="inputDescription" class="form-label">Description</label>
                            <textarea name="description" class="form-control" id="inputDescription" rows="3" placeholder="Input description">{{ old('description', $slideshow->description) }}</textarea>
                            @error('description')
                                <div class="text-danger">{{ $errors->first('description') }}</div>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="inputEmbedUrl" class="form-label">Embed Url</label>
                            <input name="embed_url" value="{{ old('embed_url', $slideshow->embed_url) }}" placeholder="Input embed url ex: https://sinurapps.com" type="text" class="form-control" id="inputEmbedUrl" aria-describedby="emailHelp">
                            @error('embed_url')
                                <div class="text-danger">{{ $errors->first('embed_url') }}</div>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <img src="{{ asset('storage/slideshows/'.$slideshow->image) }}" class="d-block img-thumbnail mb-3">
                            <label for="inputImage" class="form-label">Change Image</label>
                            <input name="image" type="file" id="inputImage" class="form-control" aria-label="file example" accept=".jpeg,.jpg,.png">
                            @error('image')
                                <div class="text-danger">{{ $errors->first('image') }}</div>
                            @enderror
                        </div>

                        <div class="row g-3 justify-content-end">
                            <div class="col-auto">
                                <a class="btn btn-outline-secondary" href="{{ route('admin.slideshow.index') }}" role="button">Cancel</a>
                            </div>
                            <div class="col-auto">
                                <button type="submit" class="btn btn-outline-primary">Save</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('styles')
    <script src="https://cdn.ckeditor.com/ckeditor5/41.4.2/classic/ckeditor.js"></script>
@endpush

@push('scripts')
<script>
    ClassicEditor.create(document.querySelector('#inputDescription')).catch(error => {
        console.error(error);
    });
</script>
@endpush
