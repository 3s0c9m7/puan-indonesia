<?php

namespace Modules\Website\Constants;

use App\Constants\BaseConstant;

class SlideshowEmbedType extends BaseConstant
{
    const URL = 1;

    public static function labels(): array
    {
        return [
            self::URL => "Link",
        ];
    }
}
