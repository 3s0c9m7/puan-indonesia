<?php

namespace Modules\Website\Constants;

use App\Constants\BaseConstant;

class BlogEmbedType extends BaseConstant
{
    const YOUTUBE = 1;

    public static function labels(): array
    {
        return [
            self::YOUTUBE => "Youtube",
        ];
    }
}
