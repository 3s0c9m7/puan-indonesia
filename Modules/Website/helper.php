<?php

if (!function_exists('notice')) {
    /**
     * fungsi untuk men-set pesan pada halaman frontend
     * @param  String $labelClass
     * @param  String $content
     * @return \Illuminate\Session\Store
     */
    function notice($labelClass, $content)
    {
        // Session::forget('notice');
        $notices = Session::get('notice');
        if (!is_array($notices))
            $notices = [];

        array_push($notices, [
            'labelClass' => $labelClass,
            'content' => $content
        ]);

        Session::put('notice', $notices);
    }
}
