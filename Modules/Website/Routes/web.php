<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::prefix('website')->group(function() {
    Route::get('/', 'WebsiteController@index');
});
*/

Route::prefix('/')->group(function() {
    Route::get('/', 'HomeController');
    Route::get('/home', 'HomeController');
});

Route::group([
    'prefix' => 'admin',
    'as' => 'admin.',
    'namespace' => 'Admin',
    'middleware' => [
        'auth',
    ]
],function() {
    Route::group([
        'prefix' => 'slideshows',
        'as' => 'slideshow.',
        'namespace' => 'Slideshows'
    ], function() {
        Route::get('/', 'IndexController')->name('index');
        Route::get('/create', 'CreateController')->name('create');
        Route::post('/store', 'StoreController')->name('store');
        Route::get('{uuid}/edit', 'EditController')->name('edit');
        Route::post('{uuid}/update', 'UpdateController')->name('update');
        Route::get('{uuid}/delete', 'DeleteController')->name('delete');
    });

    Route::group([
        'prefix' => 'master',
        'as' => 'master.',
        'namespace' => 'Master'
    ], function() {
        Route::group([
            'prefix' => 'products',
            'as' => 'product.',
            'namespace' => 'Products'
        ], function() {
            Route::get('/', 'IndexController')->name('index');
            Route::get('/create', 'CreateController')->name('create');
            Route::post('/store', 'StoreController')->name('store');
            Route::get('{uuid}/edit', 'EditController')->name('edit');
            Route::post('{uuid}/update', 'UpdateController')->name('update');
            Route::get('{uuid}/delete', 'DeleteController')->name('delete');
        });
        Route::group([
            'prefix' => 'product-type',
            'as' => 'product_type.',
            'namespace' => 'ProductType'
        ], function() {
            Route::get('/', 'IndexController')->name('index');
            Route::get('/create', 'CreateController')->name('create');
            Route::post('/store', 'StoreController')->name('store');
            Route::get('{uuid}/edit', 'EditController')->name('edit');
            Route::post('{uuid}/update', 'UpdateController')->name('update');
            Route::get('{uuid}/delete', 'DeleteController')->name('delete');
        });
        Route::group([
            'prefix' => 'collections',
            'as' => 'collection.',
            'namespace' => 'Collections'
        ], function() {
            Route::get('/', 'IndexController')->name('index');
            Route::get('/create', 'CreateController')->name('create');
            Route::post('/store', 'StoreController')->name('store');
            Route::get('{uuid}/edit', 'EditController')->name('edit');
            Route::post('{uuid}/update', 'UpdateController')->name('update');
            Route::get('{uuid}/delete', 'DeleteController')->name('delete');
        });
    });

    Route::group([
        'prefix' => 'blogs',
        'as' => 'blog.',
        'namespace' => 'Blogs'
    ], function() {
        Route::get('/', 'IndexController')->name('index');
        Route::get('/create', 'CreateController')->name('create');
        Route::post('/store', 'StoreController')->name('store');
        Route::get('{uuid}/edit', 'EditController')->name('edit');
        Route::post('{uuid}/update', 'UpdateController')->name('update');
        Route::get('{uuid}/delete', 'DeleteController')->name('delete');
    });
});
