<?php
namespace Modules\Website\Database\factories;

use Illuminate\Support\Str;
use Modules\Website\Constants\BlogEmbedType;
use Illuminate\Database\Eloquent\Factories\Factory;

class BlogFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = \Modules\Website\Repositories\Entities\Blog::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'uuid' => Str::uuid(36),
            'title' => $this->faker->realText($maxNbChars = 50, $indexSize = 2),
            'embed_url' => 'https://www.youtube.com/embed/9GfoQWHqXEA',
            'embed_type' => BlogEmbedType::YOUTUBE,
            'description' => $this->faker->realText($maxNbChars = 200, $indexSize = 2),
        ];
    }
}

