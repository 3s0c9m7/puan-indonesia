<?php
namespace Modules\Website\Database\factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;

class SlideshowFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = \Modules\Website\Repositories\Entities\Slideshow::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'uuid' => Str::uuid(36),
            'title' => $this->faker->realText($maxNbChars = 20, $indexSize = 2),
            'description' => Arr::random($this->getDescriptions(), 1)[0],
        ];
    }

    private function getDescriptions()
    {
        return [
            '
                <h1>Example headline.</h1>
                <p class="opacity-75">Some representative placeholder content for the first slide of the carousel.</p>
                <p>
                    <a class="btn btn-lg btn-primary" href="#">Sign up today</a>
                </p>
            ',
            '
                <h1>Another example headline.</h1>
                <p>Some representative placeholder content for the second slide of the carousel.</p>
                <p>
                    <a class="btn btn-lg btn-primary" href="#">Learn more</a>
                </p>
            ',
            '
                <h1>One more for good measure.</h1>
                <p>Some representative placeholder content for the third slide of this carousel.</p>
                <p>
                    <a class="btn btn-lg btn-primary" href="#">Browse gallery</a>
                </p>
            ',
        ];
    }
}

