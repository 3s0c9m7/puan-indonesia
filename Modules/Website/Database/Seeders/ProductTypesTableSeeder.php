<?php

namespace Modules\Website\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Website\Repositories\Entities\ProductType;

class ProductTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call("OthersTableSeeder");
        $productTypes = [
            ['title'=>'PUAN INDONESIA'],
            ['title'=>'PUAN VIBE'],
            ['title'=>'KUKASIH ID'],
        ];

        foreach($productTypes as $type) {
            ProductType::firstOrCreate($type);
        }
    }
}
