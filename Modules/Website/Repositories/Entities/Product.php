<?php

namespace Modules\Website\Repositories\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Repositories\Eloquent\Traits\WithUuid;

#use Hoyvoy\CrossDatabase\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Modules\Website\Repositories\Entities\ProductType;

class Product extends Model
{
    use HasFactory, SoftDeletes, WithUuid;

#    protected $connection = 'mysql_inventory_tracking';
    protected $table = 'products';

    protected $hidden = [
        'id'
    ];

    protected $guarded = [
        'id'
    ];

    public function type(): BelongsTo
    {
        return $this->belongsTo(ProductType::class, 'product_type_id');
    }
}
