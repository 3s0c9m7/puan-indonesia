<?php
namespace Modules\Website\Repositories;

use Illuminate\Support\Facades\DB;
use App\Exceptions\DeleteException;
use Modules\Website\Constants\SlideshowEmbedType;
use Modules\Website\Repositories\Entities\Slideshow;

class SlideshowRepository
{
    public function filters(array $data)
    {
        return Slideshow::when(isset($data['embed_type']), function($q) use($data){
            $q->where('embed_type', $data['embed_type']);
        })->when(isset($data['search']), function($q) use($data){
            $q->where('title','like',"%{$data['search']}%");
        });
    }

    public function paginate(array $data, int $perPage = 10)
    {
        return $this->filters($data)->latest()->paginate($perPage);
    }

    public function fetchRandom(array $data, int $limit = 3)
    {
        return $this->filters($data)->inRandomOrder()->limit($limit)->get();
    }

    public function fetch(array $data)
    {
        return $this->filters($data)->get();
    }

    public function paginateWithStock()
    {
        $db = DB::connection(conn_inventory_tracking())->getDatabaseName();
        $query = "
            pi.id,
            pi.lot_or_serial_number,
            pi.status,
            (
                (
                    SELECT SUM(m.quantity)
                    FROM {$db}.movements as m
                    WHERE m.product_item_id = pi.id
                    AND m.movement_type = 1
                )
                -(
                    SELECT SUM(m.quantity)
                    FROM {$db}.movements as m
                    WHERE m.product_item_id = pi.id
                    AND m.movement_type = 2
                )
            ) as stock_in_hand
        ";

        return DB::table("{$db}.product_items as pi")
            ->select(DB::raw($query))
            ->havingRaw('stock_in_hand > 0')
            ->paginate(10);
    }

    public function get(array $data)
    {
        return $this->filters($data)->get();
    }

    public function find(string $uuid)
    {
        return Slideshow::findOrFailByUuid($uuid);
    }

    public function store(array $data)
    {
        return Slideshow::create($data);
    }

    public function update(string $uuid, array $data)
    {
        $slideshow = $this->find($uuid)->update($data);
        return $slideshow;
    }

    public function delete(string $uuid)
    {
        $slideshow = $this->find($uuid);
        // buat exception jika mempunyai relasi dengan tabel lain
        //if ($category->otherTable()->exists()) {
        //    throw new DeleteException('Data tidak bisa dihapus, karena sebagai referensi data lain'); 
        //}
        return $slideshow->delete();
    }
}
