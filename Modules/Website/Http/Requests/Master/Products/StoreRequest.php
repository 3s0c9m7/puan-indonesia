<?php

namespace Modules\Website\Http\Requests\Master\Products;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Website\Constants\BlogEmbedType;
use Modules\Website\Repositories\ProductRepository;

class StoreRequest extends FormRequest
{
    public $product;
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required',
            'product_type_id' => 'required',
            'description' => 'required',
        ];

        if($this->uuid) {
            $this->product = (new ProductRepository)->find($this->uuid);

            if($this->product->image == empty($this->image)) {
#                unset($rules['image']);
            }
        }

        return $rules;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'product_type_id.required' => 'The product type field is required.',
        ];
    }

    public function withValidator($validator): void
    {
        if ($validator->fails()) {
            notice('error', 'Data gagal divalidasi');
        }
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function getImage()
    {

        if($this->image) {
            $imageName = time() . '.' . $this->image->extension();
            // $request->image->move(public_path('images'), $imageName);
            $this->image->storeAs('public/blogs', $imageName);

            return $imageName;
        }

        if($this->uuid) {
            return $this->blog->image;
        }

        return null;
    }

    public function getEmbedUrl()
    {

        if($this->embed_url) {
            return $this->embed_url;
        }

        if($this->uuid) {
            return $this->blog->embed_url;
        }

        return null;
    }

    public function getEmbedType()
    {

        if($this->embed_url) {
            return BlogEmbedType::YOUTUBE;
        }

        if($this->uuid) {
            return $this->blog->embed_type;
        }

        return null;
    }

    public function data()
    {
        return [
            'name' => $this->input('name'),
            'product_type_id' => $this->input('product_type_id'),
            'description' => $this->input('description'),
        ];
    }
}
