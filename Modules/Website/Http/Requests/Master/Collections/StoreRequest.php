<?php

namespace Modules\Website\Http\Requests\Master\Collections;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Website\Repositories\CollectionRepository;

class StoreRequest extends FormRequest
{
    public $collection;
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required',
        ];

        if($this->uuid) {
            $this->collection = (new CollectionRepository)->find($this->uuid);
        }

        return $rules;
    }

    public function withValidator($validator): void
    {
        if ($validator->fails()) {
            notice('error', 'Data gagal divalidasi');
        }
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function data()
    {
        return [
            'name' => $this->input('name'),
        ];
    }
}
