<?php

namespace Modules\Website\Http\Requests\Master\ProductTypes;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Website\Repositories\ProductTypeRepository;

class StoreRequest extends FormRequest
{
    public $productType;
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'required',
        ];

        if($this->uuid) {
            $this->productType = (new ProductTypeRepository)->find($this->uuid);
        }

        return $rules;
    }

    public function withValidator($validator): void
    {
        if ($validator->fails()) {
            notice('error', 'Data gagal divalidasi');
        }
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function data()
    {
        return [
            'title' => $this->input('title'),
        ];
    }
}
