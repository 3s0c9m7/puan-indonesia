<?php

namespace Modules\Website\Http\Requests\Slideshows;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Website\Constants\SlideshowEmbedType;
use Modules\Website\Repositories\SlideshowRepository;

class StoreRequest extends FormRequest
{
    public $slideshow;
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'required',
            'description' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
        ];

        if($this->uuid) {
            $this->slideshow = (new SlideshowRepository)->find($this->uuid);

            if($this->slideshow->image == empty($this->image)) {
                unset($rules['image']);
            }
        }

        return $rules;
    }

    public function withValidator($validator): void
    {
        if ($validator->fails()) {
            notice('error', 'Data gagal divalidasi');
        }
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function getImage()
    {

        if($this->image) {
            $imageName = time() . '.' . $this->image->extension();
            // $request->image->move(public_path('images'), $imageName);
            $this->image->storeAs('public/slideshows', $imageName);

            return $imageName;
        }

        if($this->uuid) {
            return $this->slideshow->image;
        }

        return null;
    }

    public function getEmbedUrl()
    {

        if($this->embed_url) {
            return $this->embed_url;
        }

        if($this->uuid) {
            return $this->slideshow->embed_url;
        }

        return null;
    }

    public function getEmbedType()
    {

        if($this->embed_url) {
            return SlideshowEmbedType::URL;
        }

        if($this->uuid) {
            return $this->slideshow->embed_type;
        }

        return null;
    }

    public function data()
    {
        return [
            'title' => $this->input('title'),
            'description' => $this->input('description'),
            'embed_url' => $this->getEmbedUrl(),
            'embed_type' => $this->getEmbedType(),
            'image' => $this->getImage(),
        ];
    }
}
