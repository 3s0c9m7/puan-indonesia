<?php

namespace Modules\Website\Http\Requests\Blogs;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Website\Constants\BlogEmbedType;
use Modules\Website\Repositories\BlogRepository;

class StoreRequest extends FormRequest
{
    public $blog;
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'required',
            'description' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
        ];

        if($this->uuid) {
            $this->blog = (new BlogRepository)->find($this->uuid);

            if($this->blog->image == empty($this->image)) {
                unset($rules['image']);
            }
        }

        return $rules;
    }

    public function withValidator($validator): void
    {
        if ($validator->fails()) {
            notice('error', 'Data gagal divalidasi');
        }
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function getImage()
    {

        if($this->image) {
            $imageName = time() . '.' . $this->image->extension();
            // $request->image->move(public_path('images'), $imageName);
            $this->image->storeAs('public/blogs', $imageName);

            return $imageName;
        }

        if($this->uuid) {
            return $this->blog->image;
        }

        return null;
    }

    public function getEmbedUrl()
    {

        if($this->embed_url) {
            return $this->embed_url;
        }

        if($this->uuid) {
            return $this->blog->embed_url;
        }

        return null;
    }

    public function getEmbedType()
    {

        if($this->embed_url) {
            return BlogEmbedType::YOUTUBE;
        }

        if($this->uuid) {
            return $this->blog->embed_type;
        }

        return null;
    }

    public function data()
    {
        return [
            'title' => $this->input('title'),
            'description' => $this->input('description'),
            'embed_url' => $this->getEmbedUrl(),
            'embed_type' => $this->getEmbedType(),
            'image' => $this->getImage(),
        ];
    }
}
