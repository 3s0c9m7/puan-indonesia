<?php

namespace Modules\Website\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Website\Repositories\SlideshowRepository;
use Modules\Website\Repositories\BlogRepository;

class HomeController extends Controller
{
    public $slideshow;
    public $blog;

    public function __construct(
        SlideshowRepository $slideshow,
        BlogRepository $blog
    ) {
        $this->slideshow = $slideshow;
        $this->blog = $blog;
    }
    /**
     * Display a home|beranda website
     * @return Renderable
     */
    public function __invoke()
    {
        $slideshows = $this->slideshow->fetch($filters = []);
        $blogs = $this->blog->fetchRandom($filters = [], $limit = 3);
        return view('website::home', compact('slideshows','blogs'));
    }
}
