<?php

namespace Modules\Website\Http\Controllers\Admin\Master\Collections;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Website\Repositories\ProductTypeRepository;
use Modules\Website\Http\Requests\Master\ProductTypes\StoreRequest;

class UpdateController extends Controller
{
    public $productType;
    public $title = 'Product Type';

    public function __construct(ProductTypeRepository $productType)
    {
        $this->productType = $productType;
    }

    /**
     * Update the specified resource in storage.
     * @param StoreRequest $request
     * @param string $uuid
     * @return Renderable
     */
    public function __invoke(StoreRequest $request, string $uuid)
    {
        try {
            $this->productType->update($uuid, $request->data());
            notice('success', "{$this->title} berhasil diubah");
        } catch (\Exception $e) {
#            panic($e);
            notice('error', "Terjadi kesalahan, silakan hubungi admin");
        }

        return redirect()->route('admin.master.product_type.index');
    }
}
