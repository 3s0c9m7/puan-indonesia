<?php

namespace Modules\Website\Http\Controllers\Admin\Master\Collections;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Website\Repositories\CollectionRepository;
use Modules\Website\Http\Requests\Master\Collections\StoreRequest;

class StoreController extends Controller
{
    public $collection;
    public $title = 'Product Type';

    public function __construct(CollectionRepository $collection)
    {
        $this->collection = $collection;
    }

    /**
     * Store a newly created resource in storage.
     * @param StoreRequest $request
     * @return Renderable
     */
    public function __invoke(StoreRequest $request)
    {
        $data = $request->data();
        try {
            $this->collection->store($data);
            notice('success', "{$this->title} berhasil ditambahkan");
        } catch (\Exception $e) {
            notice('error', "Terjadi kesalahan, silakan hubungi admin");
        }

        return redirect()->route('admin.master.collection.index');
    }
}
