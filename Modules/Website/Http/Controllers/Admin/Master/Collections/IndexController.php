<?php

namespace Modules\Website\Http\Controllers\Admin\Master\Collections;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Website\Repositories\CollectionRepository;

class IndexController extends Controller
{
    public $collection;

    public function __construct(CollectionRepository $collection)
    {
        $this->collection = $collection;
    }
    /**
     * Display a home|beranda website
     * @return Renderable
     */
    public function __invoke(Request $request)
    {
        $filters = [];
        if($request->has('search')) {
            $filters['search'] = $request->search;
        }

        $collections = $this->collection->paginate($filters = $filters, $limit = 10);
        return view('website::admin.master.collections.index', compact('collections'));
    }
}
