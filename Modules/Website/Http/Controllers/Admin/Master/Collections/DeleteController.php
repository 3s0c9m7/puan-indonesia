<?php

namespace Modules\Website\Http\Controllers\Admin\Master\Collections;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Website\Repositories\CollectionRepository;

class DeleteController extends Controller
{
    public $collection;
    public $title = 'Product Collection';

    public function __construct(CollectionRepository $collection)
    {
        $this->collection = $collection;
    }

    /**
     * Remove the specified resource from storage.
     * @param string $uuid
     * @return Renderable
     */
    public function __invoke(string $uuid)
    {
        try {
            $this->collection->delete($uuid);
            notice('success', "{$this->title} berhasil dihapus");
        } catch (\App\Exceptions\DeleteException $e) {
            notice('error', $e->getMessage());
        } catch (\Exception $e) {
#            panic($e);
            notice('error', "Terjadi kesalahan, silakan hubungi admin");
        }

        return redirect()->route('admin.master.collection.index');
    }
}
