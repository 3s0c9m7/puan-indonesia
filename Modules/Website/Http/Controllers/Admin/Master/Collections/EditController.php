<?php

namespace Modules\Website\Http\Controllers\Admin\Master\Collections;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Website\Repositories\ProductTypeRepository;

class EditController extends Controller
{
    public $productType;
    public $title = 'Product Type';

    public function __construct(ProductTypeRepository $productType)
    {
        $this->productType = $productType;
    }

    /**
     * Show the form for editing the specified resource.
     * @param string $uuid
     * @return Renderable
     */
    public function __invoke(string $uuid)
    {
        $productType = $this->productType->find($uuid);
        return view('website::admin.product-types.edit', compact('productType'));
    }
}
