<?php

namespace Modules\Website\Http\Controllers\Admin\Master\ProductType;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Website\Repositories\ProductTypeRepository;

class DeleteController extends Controller
{
    public $productType;
    public $title = 'Product Type';

    public function __construct(ProductTypeRepository $productType)
    {
        $this->productType = $productType;
    }

    /**
     * Remove the specified resource from storage.
     * @param string $uuid
     * @return Renderable
     */
    public function __invoke(string $uuid)
    {
        try {
            $this->productType->delete($uuid);
            notice('success', "{$this->title} berhasil dihapus");
        } catch (\App\Exceptions\DeleteException $e) {
            notice('error', $e->getMessage());
        } catch (\Exception $e) {
#            panic($e);
            notice('error', "Terjadi kesalahan, silakan hubungi admin");
        }

        return redirect()->route('admin.master.product_type.index');
    }
}
