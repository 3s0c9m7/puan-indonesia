<?php

namespace Modules\Website\Http\Controllers\Admin\Master\ProductType;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Website\Repositories\ProductTypeRepository;
use Modules\Website\Http\Requests\Master\ProductTypes\StoreRequest;

class StoreController extends Controller
{
    public $productType;
    public $title = 'Product Type';

    public function __construct(ProductTypeRepository $productType)
    {
        $this->productType = $productType;
    }

    /**
     * Store a newly created resource in storage.
     * @param StoreRequest $request
     * @return Renderable
     */
    public function __invoke(StoreRequest $request)
    {
        $data = $request->data();
        try {
            $this->productType->store($data);
            notice('success', "{$this->title} berhasil ditambahkan");
        } catch (\Exception $e) {
            notice('error', "Terjadi kesalahan, silakan hubungi admin");
        }

        return redirect()->route('admin.master.product_type.index');
    }
}
