<?php

namespace Modules\Website\Http\Controllers\Admin\Master\ProductType;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Website\Repositories\ProductTypeRepository;

class IndexController extends Controller
{
    public $productType;

    public function __construct(ProductTypeRepository $productType)
    {
        $this->productType = $productType;
    }
    /**
     * Display a home|beranda website
     * @return Renderable
     */
    public function __invoke(Request $request)
    {
        $filters = [];
        if($request->has('search')) {
            $filters['search'] = $request->search;
        }

        $productTypes = $this->productType->paginate($filters = $filters, $limit = 10);
        return view('website::admin.product-types.index', compact('productTypes'));
    }
}
