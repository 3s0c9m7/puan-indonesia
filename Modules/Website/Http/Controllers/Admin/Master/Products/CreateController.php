<?php

namespace Modules\Website\Http\Controllers\Admin\Master\Products;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Website\Repositories\ProductTypeRepository;

class CreateController extends Controller
{
    public $productType;
    public $title = 'Product';

    public function __construct(ProductTypeRepository $productType)
    {
        $this->productType = $productType;
    }
    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function __invoke()
    {
        $productTypes = $this->productType->fetch();

        return view(
            'website::admin.master.products.create',
            compact('productTypes')
        );
    }
}
