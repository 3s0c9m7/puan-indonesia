<?php

namespace Modules\Website\Http\Controllers\Admin\Master\Products;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Website\Repositories\ProductRepository;

class IndexController extends Controller
{
    public $product;

    public function __construct(ProductRepository $product)
    {
        $this->product = $product;
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function __invoke()
    {
        $filters = [];

        $products = $this->product->paginate($filters = $filters, $limit = 10);
        return view('website::admin.master.products.index', compact('products'));
    }
}
