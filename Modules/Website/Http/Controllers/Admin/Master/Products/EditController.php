<?php

namespace Modules\Website\Http\Controllers\Admin\Master\Products;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Website\Repositories\ProductRepository;
use Modules\Website\Repositories\ProductTypeRepository;

class EditController extends Controller
{
    public $product;
    public $productType;
    public $title = 'Product';

    public function __construct(
        ProductRepository $product,
        ProductTypeRepository $productType
    ) {
        $this->product = $product;
        $this->productType = $productType;
    }

    /**
     * Show the form for editing the specified resource.
     * @param string $uuid
     * @return Renderable
     */
    public function __invoke(string $uuid)
    {
        $product = $this->product->find($uuid);
        $productTypes = $this->productType->fetch();

        return view(
            'website::admin.master.products.edit',
            compact('product','productTypes')
        );
    }
}
