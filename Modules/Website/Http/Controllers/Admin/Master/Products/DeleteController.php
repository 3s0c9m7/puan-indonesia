<?php

namespace Modules\Website\Http\Controllers\Admin\Master\Products;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Website\Repositories\ProductRepository;

class DeleteController extends Controller
{
    public $product;
    public $title = 'Product';

    public function __construct(ProductRepository $product)
    {
        $this->product = $product;
    }

    /**
     * Remove the specified resource from storage.
     * @param string $uuid
     * @return Renderable
     */
    public function __invoke(string $uuid)
    {
        try {
            $this->product->delete($uuid);
            notice('success', "{$this->title} berhasil dihapus");
        } catch (\App\Exceptions\DeleteException $e) {
            notice('error', $e->getMessage());
        } catch (\Exception $e) {
#            panic($e);
            notice('error', "Terjadi kesalahan, silakan hubungi admin");
        }

        return redirect()->route('admin.master.product.index');
    }
}
