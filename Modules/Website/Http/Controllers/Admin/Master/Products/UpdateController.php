<?php

namespace Modules\Website\Http\Controllers\Admin\Master\Products;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Website\Repositories\ProductRepository;
use Modules\Website\Http\Requests\Master\Products\StoreRequest;

class UpdateController extends Controller
{
    public $product;
    public $title = 'Product';

    public function __construct(ProductRepository $product)
    {
        $this->product = $product;
    }

    /**
     * Update the specified resource in storage.
     * @param StoreRequest $request
     * @param string $uuid
     * @return Renderable
     */
    public function __invoke(StoreRequest $request, string $uuid)
    {
        try {
            $this->product->update($uuid, $request->data());
            notice('success', "{$this->title} berhasil diubah");
        } catch (\Exception $e) {
#            panic($e);
            notice('error', "Terjadi kesalahan, silakan hubungi admin");
        }

        return redirect()->route('admin.master.product.index');
    }
}
