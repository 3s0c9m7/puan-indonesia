<?php

namespace Modules\Website\Http\Controllers\Admin\Master\Products;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Website\Repositories\ProductRepository;
use Modules\Website\Http\Requests\Master\Products\StoreRequest;

class StoreController extends Controller
{
    public $product;
    public $title = 'Product';

    public function __construct(ProductRepository $product)
    {
        $this->product = $product;
    }

    /**
     * Store a newly created resource in storage.
     * @param StoreRequest $request
     * @return Renderable
     */
    public function __invoke(StoreRequest $request)
    {
        $data = $request->data();
        try {
            $this->product->store($data);
            notice('success', "{$this->title} berhasil ditambahkan");
        } catch (\Exception $e) {
            notice('error', "Terjadi kesalahan, silakan hubungi admin");
        }

        return redirect()->route('admin.master.product.index');
    }
}
