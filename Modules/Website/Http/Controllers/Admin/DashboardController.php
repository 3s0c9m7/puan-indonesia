<?php

namespace Modules\Website\Http\Controllers\Admin;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class DashboardController extends Controller
{
    /**
     * Display a home|beranda website
     * @return Renderable
     */
    public function __invoke()
    {
        return view('website::admin.dashboard');
    }
}
