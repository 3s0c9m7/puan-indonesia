<?php

namespace Modules\Website\Http\Controllers\Admin\Slideshows;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Website\Repositories\SlideshowRepository;

class EditController extends Controller
{
    public $slideshow;
    public $title = 'Slideshow';

    public function __construct(SlideshowRepository $slideshow)
    {
        $this->slideshow = $slideshow;
    }

    /**
     * Show the form for editing the specified resource.
     * @param string $uuid
     * @return Renderable
     */
    public function __invoke(string $uuid)
    {
        $slideshow = $this->slideshow->find($uuid);
        return view('website::admin.slideshows.edit', compact('slideshow'));
    }
}
