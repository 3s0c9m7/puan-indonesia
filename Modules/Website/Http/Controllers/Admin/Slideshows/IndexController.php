<?php

namespace Modules\Website\Http\Controllers\Admin\Slideshows;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Website\Repositories\SlideshowRepository;

class IndexController extends Controller
{
    public $slideshow;

    public function __construct(SlideshowRepository $slideshow)
    {
        $this->slideshow = $slideshow;
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function __invoke()
    {
        $filters = [];

        $slideshows = $this->slideshow->paginate($filters = $filters, $limit = 10);
        return view('website::admin.slideshows.index', compact('slideshows'));
    }
}
