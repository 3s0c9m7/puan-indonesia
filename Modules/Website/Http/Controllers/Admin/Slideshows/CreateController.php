<?php

namespace Modules\Website\Http\Controllers\Admin\Slideshows;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class CreateController extends Controller
{
    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function __invoke()
    {
        return view('website::admin.slideshows.create');
    }
}
