<?php

namespace Modules\Website\Http\Controllers\Admin\Slideshows;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Website\Repositories\SlideshowRepository;
use Modules\Website\Http\Requests\Slideshows\StoreRequest;

class UpdateController extends Controller
{
    public $slideshow;
    public $title = 'Slideshow';

    public function __construct(SlideshowRepository $slideshow)
    {
        $this->slideshow = $slideshow;
    }

    /**
     * Update the specified resource in storage.
     * @param StoreRequest $request
     * @param string $uuid
     * @return Renderable
     */
    public function __invoke(StoreRequest $request, string $uuid)
    {
        try {
            $this->slideshow->update($uuid, $request->data());
            notice('success', "{$this->title} berhasil diubah");
        } catch (\Exception $e) {
#            panic($e);
            notice('error', "Terjadi kesalahan, silakan hubungi admin");
        }

        return redirect()->route('admin.slideshow.index');
    }
}
