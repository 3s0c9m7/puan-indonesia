<?php

namespace Modules\Website\Http\Controllers\Admin\Slideshows;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Website\Repositories\SlideshowRepository;

class DeleteController extends Controller
{
    public $slideshow;
    public $title = 'Slideshow';

    public function __construct(SlideshowRepository $slideshow)
    {
        $this->slideshow = $slideshow;
    }

    /**
     * Remove the specified resource from storage.
     * @param string $uuid
     * @return Renderable
     */
    public function __invoke(string $uuid)
    {
        try {
            $this->slideshow->delete($uuid);
            notice('success', "{$this->title} berhasil dihapus");
        } catch (\App\Exceptions\DeleteException $e) {
            notice('error', $e->getMessage());
        } catch (\Exception $e) {
#            panic($e);
            notice('error', "Terjadi kesalahan, silakan hubungi admin");
        }

        return redirect()->route('admin.slideshow.index');
    }
}
