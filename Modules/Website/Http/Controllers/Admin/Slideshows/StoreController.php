<?php

namespace Modules\Website\Http\Controllers\Admin\Slideshows;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Website\Repositories\SlideshowRepository;
use Modules\Website\Http\Requests\Slideshows\StoreRequest;

class StoreController extends Controller
{
    public $slideshow;
    public $title = 'Slideshow';

    public function __construct(SlideshowRepository $slideshow)
    {
        $this->slideshow = $slideshow;
    }

    /**
     * Store a newly created resource in storage.
     * @param StoreRequest $request
     * @return Renderable
     */
    public function __invoke(StoreRequest $request)
    {
        $data = $request->data();
        try {
            $this->slideshow->store($data);
            notice('success', "{$this->title} berhasil ditambahkan");
        } catch (\Exception $e) {
            notice('error', "Terjadi kesalahan, silakan hubungi admin");
        }

        return redirect()->route('admin.slideshow.index');
    }
}
