<?php

namespace Modules\Website\Http\Controllers\Admin\Blogs;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Website\Repositories\BlogRepository;

class IndexController extends Controller
{
    public $blog;

    public function __construct(BlogRepository $blog)
    {
        $this->blog = $blog;
    }
    /**
     * Display a home|beranda website
     * @return Renderable
     */
    public function __invoke(Request $request)
    {
        $filters = [];
        if($request->has('search')) {
            $filters['search'] = $request->search;
        }

        $blogs = $this->blog->paginate($filters = $filters, $limit = 10);
        return view('website::admin.blogs.index', compact('blogs'));
    }
}
