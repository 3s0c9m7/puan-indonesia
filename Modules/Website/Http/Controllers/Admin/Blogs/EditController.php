<?php

namespace Modules\Website\Http\Controllers\Admin\Blogs;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Website\Repositories\BlogRepository;

class EditController extends Controller
{
    public $blog;
    public $title = 'Blog';

    public function __construct(BlogRepository $blog)
    {
        $this->blog = $blog;
    }
    /**
     * Show the form for editing the specified resource.
     * @param string $uuid
     * @return Renderable
     */
    public function __invoke(string $uuid)
    {
        $blog = $this->blog->find($uuid);
        return view('website::admin.blogs.edit', compact('blog'));
    }
}
