<?php

namespace Modules\Website\Http\Controllers\Admin\Blogs;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Website\Repositories\BlogRepository;
use Modules\Website\Http\Requests\Blogs\StoreRequest;

class UpdateController extends Controller
{
    public $blog;
    public $title = 'Blog';

    public function __construct(BlogRepository $blog)
    {
        $this->blog = $blog;
    }
    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function __invoke(StoreRequest $request, string $uuid)
    {
        try {
            $this->blog->update($uuid, $request->data());
            notice('success', "{$this->title} berhasil diubah");
        } catch (\Exception $e) {
#            panic($e);
            notice('error', "Terjadi kesalahan, silakan hubungi admin");
        }

        return redirect()->route('admin.blog.index');
    }
}
