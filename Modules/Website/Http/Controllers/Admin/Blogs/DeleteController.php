<?php

namespace Modules\Website\Http\Controllers\Admin\Blogs;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Website\Repositories\BlogRepository;

class DeleteController extends Controller
{
    public $blog;
    public $title = 'Blog';

    public function __construct(BlogRepository $blog)
    {
        $this->blog = $blog;
    }
    /**
     * Remove the specified resource from storage.
     * @param string $uuid
     * @return Renderable
     */
    public function __invoke(string $uuid)
    {
        try {
            $this->blog->delete($uuid);
            notice('success', "{$this->title} berhasil dihapus");
        } catch (\App\Exceptions\DeleteException $e) {
            notice('error', $e->getMessage());
        } catch (\Exception $e) {
#            panic($e);
            notice('error', "Terjadi kesalahan, silakan hubungi admin");
        }

        return redirect()->route('admin.blog.index');
    }
}
