<?php

namespace Modules\Website\Http\Controllers\Admin\Blogs;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Website\Repositories\BlogRepository;
use Modules\Website\Http\Requests\Blogs\StoreRequest;

class StoreController extends Controller
{
    public $blog;
    public $title = 'Blog';

    public function __construct(BlogRepository $blog)
    {
        $this->blog = $blog;
    }
    /**
     * Display a home|beranda website
     * @return Renderable
     */
    public function __invoke(StoreRequest $request)
    {
        $data = $request->data();
        try {
            $this->blog->store($data);
            notice('success', "{$this->title} berhasil ditambahkan");
        } catch (\Exception $e) {
            notice('error', "Terjadi kesalahan, silakan hubungi admin");
        }

        return redirect()->route('admin.blog.index');
    }
}
